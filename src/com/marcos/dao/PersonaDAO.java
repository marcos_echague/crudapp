package com.marcos.dao;

import com.marcos.bean.Persona;

public interface PersonaDAO {

	public boolean crear(Persona persona);
	
	public boolean actualizar(Persona persona);
	
	public boolean eliminar(Persona persona);
	
	public Persona consultar(String nombre);
	
	public boolean setUp(String url, String driver, String user, String password);
	
	public boolean disconnect();
	
	
}
