package com.marcos.bean;

public class Persona {

	private String nombre;
	private String direccion;
	private String telefono;
	private Integer edad;
	private Integer nivelAmistad;
	private String comentarios;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public Integer getNivelAmistad() {
		return nivelAmistad;
	}
	public void setNivelAmistad(Integer nivelAmistad) {
		this.nivelAmistad = nivelAmistad;
	}
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
}
